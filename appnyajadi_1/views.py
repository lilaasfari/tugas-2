from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect    
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import Message_Form, Comment_Form, Lowongan_Form
from .models import Forum, Company, Comment

# Create your views here.
response={}
def index(request):    
    response['author'] = "Rama Widragama Putra dan Anas" #TODO Implement yourname
    response['lowongan_form'] = Lowongan_Form
    page = request.GET.get('page', 1)
    
    list_forum = Forum.objects.all()
    list_comment = Comment.objects.all()
    paginate_forum = paginate(page ,list_forum)
    response['form_list'] = paginate_forum['data']
    response['comment_list'] = list_comment
    response['page_range'] = paginate_forum['page_range']
    html = 'tugas2/tugas2.html'
    return render(request, html, response)

def paginate(request, forum_list):
    # Bagian paginator
    paginator = Paginator(forum_list, 1)

    try:
        forum_new = paginator.page(request)
    except PageNotAnInteger:
        forum_new = paginator.page(1)
    except EmptyPage:
        forum_new = paginator.page(paginator.num_pages)

    index = forum_new.number - 1

    max_index = len(paginator.page_range)

    start_index = index if index >= 10 else 0
    end_index = 1 if index < max_index - 10 else max_index

    page_range = list(paginator.page_range)[start_index:end_index]

    data_paginate = {'data': forum_new, 'page_range': page_range}
    return data_paginate

def add_comment(request):
    form = Lowongan_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['nama'] = request.POST['nama']
        response['description'] = request.POST['description']
        comment = Comment(nama=response['nama'],description=response['description'], forum=Forum.objects.get(id=1))
        comment.save()
        return HttpResponseRedirect('/appnyajadi-1/')
    else:
        return HttpResponseRedirect('/appnyajadi-1/')
#############   index lila
#def index(request):
    # print ("#==> masuk index")
  #  if 'user_login' in request.session:
   #     return HttpResponseRedirect(reverse('forumku:dashboard'))
   # else:
    #    response['author'] = get_data_user(request, 'user_login')
     #   html = 'fitur_forum/login.html'
      #  return render(request, html, response)


def dashboard(request):

   # if not 'login' in request.session:
        # nanti ganti linknya dengan halaman login
    #    print("GAGAL")
   #     return HttpResponseRedirect(reverse('appnyajadi-1:index'))
    
   # if not 'user_login' in request.session.keys():
     #   return HttpResponseRedirect(reverse('appnyajadi-1:index'))
  #  else:

        #nama = request.session['login']
        #kode_identitas = get_data_user(request, 'kode_identitas')
        #try:
        company = Company.objects.get(kode_identitas = kode_identitas)
        #except Exception as e:
        #    pengguna = create_new_user(request)

        forum = Forum.objects.filter(company=company)
        
        response['message'] = message
        response['message_form'] = Message_Form
        response["message_list"] = message
       
        html = 'tugas2/fitur_forum.html'
        
        message_list = message
        paginator = Paginator(message_list, 5)
        page = request.GET.get('page', 1)
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            users = paginator.page(1)
        except EmptyPage:
            users = paginator.page(paginator.num_pages)
       
        response["message_list"] = users
        #response["nama"] = nama
        response["forum"] = forum
        html = 'tugas2/fitur_forum.html'
        return render(request, html, response)

def add_forum(request, id):
    response['id'] = id
    form = Message_Form(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        nama = request.POST['nama']
        isi = request.POST['isi']
        nomor = request.POST['nomor']

       # kode_identitas = get_data_user(request, 'kode_identitas')
        #try:
        pengguna = Pengguna.objects.get(company_name = nama)
        # except Exception as e:
        #    pengguna = create_new_user(request)  
     
        response['title'] = request.POST['title'] 
        response['message'] = request.POST['message']
        message = Message(pengguna=pengguna,title=response['title'], message=response['message'], kode_message = id)
        message.save()
        response['id'] = id

        forum = Forum()
        forum.pengguna = pengguna
        forum.isi = isi
        forum.nomor = nomor
        forum.save()

        html ='tugas2/fitur_forum.html'    
        return HttpResponseRedirect('/appnyajadi-1/')


def paginate_page(page, data_list):
    paginator = Paginator(data_list, 5)

    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    # Get the index of the current page
    index = data.number - 1
    # This value is maximum index of your pages, so the last page - 1
    max_index = len(paginator.page_range)
    # You want a range of 10, so lets calculate where to slice the list
    start_index = index if index >= 5 else 0
    end_index = 5 if index < max_index - 5 else max_index
    # Get our new page range. In the latest versions of Django page_range returns 
    # an iterator. Thus pass it to list, to make our slice possible again.
    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data':data, 'page_range':page_range}



def delete_forum(request, id_status):
    forum = Message.objects.get(pk = id_status)
    forum.delete()
    return HttpResponseRedirect('/appnyajadi-1/')