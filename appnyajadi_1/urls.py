from django.conf.urls import url
from .views import index, add_forum, dashboard, delete_forum, add_comment
from .custom_auth import auth_login, auth_logout

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add_forum/(?P<id>.*)/$', add_forum, name='add_forum'),
	url(r'^dashboard', dashboard, name='dashboard'),
	url(r'^add_comment', add_comment, name = 'add_comment'),

	#url(r'^clear/(?P<object_id>[0-9]+)', view=clear, name='clear'),
	url(r'^delete_forum/(?P<id_status>\d+)/$', delete_forum, name = 'delete_forum'),

	# custom auth
	#url(r'^custom_auth/login/$', auth_login, name='auth_login'),
	#url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
]
       
    
