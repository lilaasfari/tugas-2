# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Company(models.Model):
    company_name = models.CharField(max_length=50, default='DEFAULT VALUE')
    specialty = models.CharField(max_length=250, default='DEFAULT VALUE')
    yearFounded = models.CharField(max_length=5, default='DEFAULT VALUE')
    description = models.CharField(max_length=300, default='DEFAULT VALUE')
    website = models.CharField(max_length=300, default='DEFAULT VALUE')
    company_type = models.CharField(max_length=300, default='DEFAULT VALUE')
    url_foto = models.CharField(max_length=300, default='DEFAULT VALUE')

class Forum(models.Model):
    pengguna = models.ForeignKey(Company, default=1)
    isi = models.CharField('isi', max_length=400, default='DEFAULT VALUE')

class Comment(models.Model):
    forum = models.ForeignKey(Forum, default=1)
    nama = models.CharField('Nama', max_length=30, default='DEFAULT VALUE')
    description = models.TextField()


        
