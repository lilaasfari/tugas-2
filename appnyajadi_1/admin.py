from django.contrib import admin
from .models import Company, Forum, Comment
# Register your models here.

admin.site.register(Company)
admin.site.register(Forum)
admin.site.register(Comment)
